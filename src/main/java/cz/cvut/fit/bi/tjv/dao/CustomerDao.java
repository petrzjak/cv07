package cz.cvut.fit.bi.tjv.dao;

import cz.cvut.fit.bi.tjv.model.Customer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CustomerDao {

    @PersistenceContext
    private EntityManager em;

    public void persist(Customer customer) {
        em.persist(customer);
    }

    public List<Customer> findAll() {
        return em.createQuery("SELECT p FROM Customer p", Customer.class).getResultList();
    }

}